window.onload = function(){ 
		var chart 
		chart = new CanvasJS.Chart('chartContainer',{
		animationEnabled:true,
		theme:'light2',
		title:{
			text:'Monthly Crop Growth'
		},
		axisY:{
			includeZero:true
		},
		data:[{
			type:'column',
			dataPoints: [
				{y:200,label:'January'},
				{y:270,label:'February'},
				{y:340,label:'March'},
				{y:450,label:'April'},
				{y:410,label:'June'},
				{y:405,label:'July'},
				{y:390,label:'August'},
				{y:380,label:'September'},
				{y:300,label:'October'},
				{y:250,label:'November'},
				{y:230,label:'December'}
			]
		}]
	})
	chart.render();
};


